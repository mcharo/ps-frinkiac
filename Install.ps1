function Install-LocalModule
{
    [CmdletBinding()]
    param(
        $ModuleName,
        $ModulePath=$PSScriptRoot
    )
    function Import-PSD
    {
        [CmdletBinding()]
        param(
            $Path
        )
        $Base = Split-Path -Path $Path -Parent
        $Leaf = Split-Path -Path $Path -Leaf
        Write-Verbose "Base: $Base"
        Write-Verbose "Leaf: $Leaf"
        $Manifest = Import-LocalizedData -Filename $Leaf -BaseDirectory $Base
        $Manifest
    }

    $ModuleFolder = Join-Path $Env:UserProfile "Documents\WindowsPowerShell\Modules\$ModuleName"
    if (!$env:UserProfile)
    {
        $env:UserProfile = Join-Path '/Users' $env:USER
    }
    $ModuleFolder = $env:PSMODULEPATH.Split(':') | Where { $_ -match $env:UserProfile } | Select -First 1
    $ModuleFolder = Join-Path $ModuleFolder $ModuleName

    Remove-Module $ModuleName -ErrorAction SilentlyContinue
    $PsdPath = Join-Path $ModulePath "$ModuleName.psd1"
    Write-Verbose "PSD Path: $PsdPath"
    $PsmPath = Join-Path $ModulePath "$ModuleName.psm1"
    if ((Test-Path $PsdPath) -and (Test-Path $PsmPath))
    {
        $ModuleFound = $true
        $ManifestVersion = [version](Import-PSD -Path $PsdPath -Verbose).ModuleVersion
        $ModuleVersion = (Get-Module -ListAvailable $ModuleName).Version
        $NeedsUpdate = $ManifestVersion -gt $ModuleVersion
    }
    if ($ModuleFound)
    {
        if ($NeedsUpdate)
        {
            New-Item $ModuleFolder -ItemType Directory -ErrorAction SilentlyContinue | Out-Null

            $InstalledPsdPath = Join-Path $ModuleFolder "$ModuleName.psd1"
            $InstalledPsmPath = Join-Path $ModuleFolder "$ModuleName.psm1"
            Write-Verbose $InstalledPsdPath

            Copy-Item $PsdPath $InstalledPsdPath -Force
            Copy-Item $PsmPath $InstalledPsmPath -Force
            try
            {
                Import-Module $ModuleName
                Write-Host "Successfully installed $ModuleName module."
            }
            catch
            {
                Import-Module (Join-Path $ModulePath "$ModuleName.psm1")
            }
        }
        else # No update needed
        {
            Import-Module $ModuleName
        }

    }
    else
    {
        Write-Host "Unable to find $ModuleName module."
    }
}

Install-LocalModule -ModuleName PSFrinkiac -Verbose
Install-LocalModule -ModuleName PSImageViewer
