﻿New-Variable -Name SITE_URL -Value 'https://frinkiac.com' -Option Constant
New-Variable -Name API_URL  -Value "$SITE_URL/api" -Option Constant

function Get-FrinkiacImageUrl
{
    <#
    .SYNOPSIS
        Builds Frinkiac image url

    .DESCRIPTION
        Builds Frinkiac image url from specified parameters and SITE_URL constant

    .PARAMETER Episode
        The Simpsons episode: S01E03

    .PARAMETER Timestamp
        The 6-digit timestamp returned from the Frinkiac search API

    .EXAMPLE
        PS > Get-FrinkiacImageUrl -Episode S01E03 -Timestamp 300202

    .OUTPUTS
        [System.String]. URL of Frinkiac image from episode and timestamp
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Episode,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Timestamp
    )
    if ($Episode -and $Timestamp)
    {
        "$SITE_URL/img/$Episode/$Timestamp.jpg"
    }
}

function Get-FrinkiacCaptionUrl
{
    <#
    .SYNOPSIS
        Builds caption URL

    .DESCRIPTION
        Builds caption URL from specified parameters and API_URL constant

    .PARAMETER Episode
        The Simpsons episode: S01E03

    .PARAMETER Timestamp
        The 6-digit timestamp returned from the Frinkiac search API

    .EXAMPLE
        PS > Get-FrinkiacCaptionUrl -Episode S01E03 -Timestamp 300202

    .OUTPUTS
        [System.String]. URL of Frinkiac caption from episode and timestamp
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Episode,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Timestamp
    )
    "$API_URL/caption?e=$Episode&t=$Timestamp"
}

function Get-FrinkiacMemeUrl
{
    <#
    .SYNOPSIS
        Builds meme URL

    .DESCRIPTION
        Builds meme URL from specified parameters and SITE_URL constant

    .PARAMETER Episode
        The Simpsons episode: S01E03

    .PARAMETER Timestamp
        The 6-digit timestamp returned from the Frinkiac search API

    .PARAMETER Caption
        The caption text to encode in the returned URL

    .PARAMETER Open
        Optional switch, if specified url will be opened with default http handler

    .EXAMPLE
        PS > Get-FrinkiacMemeUrl -Episode S01E03 -Timestamp 300202 -Caption "D'oh" -Open

    .OUTPUTS
        [System.String]. URL of Frinkiac meme from episode and timestamp
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Episode,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Timestamp,
        [Parameter(ValueFromPipelineByPropertyName=$true)]
        [string]$Caption,
        [switch]$Open
    )
    if ($Episode -and $Timestamp)
    {
        if (-Not $Caption)
        {
            $Caption = ''
        }
        $Caption64 = ConvertTo-Base64 -Caption $Caption
        $MemeUrl = "$SITE_URL/meme/$Episode/$Timestamp.jpg?b64lines=$Caption64"
        if ($Open)
        {
            start $MemeUrl
        }
        $MemeUrl
    }
}

function Get-FrinkiacGifUrl
{
    <#
    .SYNOPSIS
        Builds gif URL

    .DESCRIPTION
        Builds gif URL from specified parameters and SITE_URL constant

    .PARAMETER Episode
        The Simpsons episode: S01E03

    .PARAMETER Timestamp
        The 6-digit timestamp returned from the Frinkiac search API

    .PARAMETER Caption
        The caption text to encode in the returned URL

    .PARAMETER DurationMs
        The duration of the gif in milliseconds, defaults to 1500

    .PARAMETER Open
        Optional switch, if specified url will be opened with default http handler

    .EXAMPLE
        PS > Get-FrinkiacGifUrl -Episode S01E03 -Timestamp 300202 -Caption "D'oh" -Open

    .OUTPUTS
        [System.String]. URL of Frinkiac meme from episode and timestamp
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Episode,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Timestamp,
        [Parameter(ValueFromPipelineByPropertyName=$true)]
        $Caption,
        [ValidateRange(100,4000)]
        $DurationMs=1500,
        [switch]$Open
    )
    $Timestamp += 500
    $Caption64 = ConvertTo-Base64 -Caption $Caption
    $GifUrl = "$SITE_URL/gif/$Episode/$Timestamp/$($Timestamp + $DurationMs).gif?b64lines=$Caption64"
    if ($Open)
    {
        start $GifUrl
    }
    $GifUrl
}

function Find-FrinkiacImage
{
    <#
    .SYNOPSIS
        Queries Frinkiac for specified query

    .DESCRIPTION
        Queries Frinkiac for specified query and returns first result

    .PARAMETER Query
        Search query, must be dialogue from the show

    .EXAMPLE
        PS > Find-FrinkiacImage -Query "D'oh"

    .OUTPUTS
        [System.Object]. Object with ID, Episode, and Timestamp matching specified query
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $Query
    )
    $SearchUrl = "$API_URL/search?q=$Query"
    $Results = Invoke-RestMethod -Uri $SearchUrl -Method Get
    $Results[0]
}

function Get-FrinkiacCaptions
{
    <#
    .SYNOPSIS
        Builds custom object with episode, timestamp, and caption

    .DESCRIPTION
        Builds custom object with episode, timestamp, and caption.  Caption is word-wrapped to fit image bounds

    .PARAMETER Episode
        The Simpsons episode: S01E03

    .PARAMETER Timestamp
        The 6-digit timestamp returned from the Frinkiac search API

    .EXAMPLE
        PS > Get-FrinkiacCaptions -Episode S01E03 -Timestamp 300202

    .OUTPUTS
        [System.Object]. URL of Frinkiac meme from episode and timestamp
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Episode,
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true)]
        $Timestamp
    )
    if ($Episode -and $Timestamp)
    {
        $CaptionUrl = Get-FrinkiacCaptionUrl -Episode $Episode -Timestamp $Timestamp
        Write-Verbose $CaptionUrl
        $Captions = Invoke-RestMethod -Uri $CaptionUrl -Method Get

        $FrinkiacObject = "" | Select Episode, Timestamp, Caption
        $FrinkiacObject.Episode = $Episode
        $FrinkiacObject.Timestamp = $Timestamp
        $FrinkiacObject.Caption = $Captions.Subtitles.Content -join ' '
        return $FrinkiacObject
    }
}

function ConvertTo-WordWrap
{
    <#
    .SYNOPSIS
        Builds array of from string of words.  Each array member is shorter or equal to specified length

    .DESCRIPTION
        Builds array of from string of words.  Each array member is shorter or equal to specified length

    .PARAMETER Caption
        String to word wrap

    .PARAMETER Length
        The maximum length of each line

    .EXAMPLE
        PS > ConvertTo-WordWrap -InputString 'REMEMBER WHEN I TOOK THAT HOME WINE-MAKING COURSE AND I FORGOT HOW TO DRIVE?' -Length 27

    .OUTPUTS
        [String[]]. Array of strings
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $InputString,
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $Length
    )
    $InputStringWords = $InputString.Split(' ')
    $Wrapped = @('')
    $WrappedIndex = 0
    ForEach($Word in $InputStringWords)
    {
        if (($Wrapped[$WrappedIndex].Length + $Word.Length + 1) -le $Length)
        {
            $Wrapped[$WrappedIndex] += "$Word "
        }
        else
        {
            $Wrapped += ""
            $WrappedIndex++
            $Wrapped[$WrappedIndex] += "$Word "
        }
    }
    $Wrapped
}

function ConvertTo-Base64
{
    <#
    .SYNOPSIS
        Base64 encodes string

    .DESCRIPTION
        Base64 encodes string

    .PARAMETER Caption
        String to encode

    .EXAMPLE
        PS > ConvertTo-Base64

    .OUTPUTS
        [System.String] Base64 encoded string
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $Caption
    )
    $CaptionLines = ConvertTo-WordWrap -InputString $Caption -Length 27
    $CaptionLines = $CaptionLines -join "`n"
    $Caption64 = [System.Text.Encoding]::UTF8.GetBytes($CaptionLines)
    $Caption64 = [System.Convert]::ToBase64String($Caption64)
    $Caption64
}

function New-Frinkiac
{
    <#
    .SYNOPSIS
        Searches Frinkiac for specified quote and returns image/gif

    .DESCRIPTION
        Searches Frinkiac for specified quote and returns image/gif

    .PARAMETER Query
        Quote to search Frinkiac for

    .PARAMETER Gif
        Optional switch. If specified, gif URL is returned

    .EXAMPLE
        PS > New-Frinkiac -Query 'wine-making' -Gif

    .OUTPUTS
        [System.String] Image/gif URL
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $Query,
        [switch]$Gif
    )
    $Result = Find-FrinkiacImage $Query
    if ($Result)
    {
        $AddCaptions = $Result | Get-FrinkiacCaptions
        if ($Gif)
        {
            $OutUrl = $AddCaptions | Get-FrinkiacGifUrl
        }
        else
        {
            $OutUrl = $AddCaptions | Get-FrinkiacMemeUrl
        }
        if ($OutUrl)
        {
            try
            {
                Start $OutUrl
            }
            catch
            {
                open $OutUrl
            }
        }
    }
}

Export-ModuleMember -Function New-Frinkiac
