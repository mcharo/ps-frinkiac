function New-PictureBox
{
    <#
    .SYNOPSIS
        Display image/gif

    .DESCRIPTION
        Display image/gif in Windows Form

    .PARAMETER ImageUrl
        URL of image to load into the window
        
    .EXAMPLE
        PS > New-PictureBox -ImageUrl 'https://frinkiac.com/meme/S07E16/265031.jpg' 
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $ImageUrl
    )
    Add-Type -AssemblyName System.Windows.Forms
    $Form = New-Object System.Windows.Forms.Form
    $Form.BackColor = [System.Drawing.Color]::Black
    $Form.AutoSize = $true
    $Form.Text = "Image Viewer"
    $PictureBox = New-Object System.Windows.Forms.PictureBox
    $PictureBox.Load($ImageUrl)
    $PictureBox.SizeMode = [System.Windows.Forms.PictureBoxSizeMode]::AutoSize
    $Form.Controls.Add($PictureBox)
    $Form.ShowDialog() | Out-Null
}