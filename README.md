## PSFrinkiac
Get image/gif from http://frinkiac.com based on quote

### Requirements:
- PowerShell 4.0

### Usage
#### Setup
- Install the modules in your user modules path by running `.\Install.ps1`

#### Examples
- New Frinkiac image:
    - `New-Frinkiac 'cromulent'`

- New Frinkiac gif:
    - `New-Frinkiac 'embiggen' -Gif`